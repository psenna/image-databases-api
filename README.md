# Image Databases api

[![pipeline status](https://gitlab.com/psenna/image-databases-api/badges/main/pipeline.svg)](https://gitlab.com/psenna/image-databases-api/-/commits/main)
[![coverage report](https://gitlab.com/psenna/image-databases-api/badges/main/coverage.svg)](https://gitlab.com/psenna/image-databases-api/-/commits/main)


Api para a gerencia de imagens em bases de dados


## Documentação

https://psenna.gitlab.io/image-databases-api/

## Bibliotecas utilizadas

* [FastApi](https://fastapi.tiangolo.com/)
* [Ormar(ORM)](https://collerek.github.io/ormar/)
* [Alembique(DB Migrations)](https://alembic.sqlalchemy.org/en/latest/)
